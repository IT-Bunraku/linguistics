#-*- coding: utf-8 -*-

from gestalt.core.helpers import *

from .abstract import *

###################################################################################

@extend_reactor('nltk')
class Extension(NLP_Extension):
    def parse(self, *args, **kwargs):
        return self.task('nlp_sentence', *args, **kwargs)

    def process(self, *args, **kwargs):
        return self.task('nlp_sentence', *args, **kwargs)

    #***************************************************************************

    def flatten(self, item):
        lst,obj = [], item

        while len(obj)==2:
            obj, val = obj[0], obj[1]

            lst += [val]

        return obj, lst

