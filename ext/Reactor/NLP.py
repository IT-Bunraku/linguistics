#-*- coding: utf-8 -*-

from gestalt.core.helpers import *

from .abstract import *

###################################################################################

@extend_reactor('nlp')
class Extension(NLP_Extension):
    def task(self, handler, *args, **kwargs):
        if type(handler) in (str, unicode):
            callback = {
                '': None,
            }.get(handler, None)

            if callable(callback):
                handler = callback
            else:

                for alias,task in self.reactor.rq.tasks.iteritems():
                    if handler==alias:
                        handler = task

        if callable(handler):
            if hasattr(handler, 'delay'):
                cb = handler.delay(*args, **kwargs)

                return cb.wait()
            else:
                return handler(*args, **kwargs)

    #***************************************************************************

    def understand(self, request, context, data):
        vortex = quepy.install("gestalt.que.dbpedia")

        results = {}

        results['target'], results['query'], results['meta'] = vortex.get_query(data['statement'])

        results['query'] = results['query'].replace('\n', '\n\n')

        while results['query'].startswith('\n'):
            results['query'] = results['query'][1:]

        results['cols'], results['rows'] = Reactor.sparql.table('http://dbpedia.org/sparql', results['query'])

        context['results'] = results

        return context

    #***************************************************************************

    def interpret(self, request, context, data):
        context['results'] = Reactor.nlp.parse('en', data['statement'])

        context['grammars'] = [
            dict(name='minimal', corpus='brown', rules=[
                'NN: {<NN><NN>}',
                'NP: {<DT>?<JJ>*<NN>}',
                'CHUNK: {<V.*> <TO> <V.*>}',
            ]),
            dict(name='default', corpus='brown', rules=[
                'NP:'
                '{<.*>+}          # Chunk everything'
                '}<VBD|IN>+{      # Chink sequences of VBD and IN'
            ]),
        ]

        context['results']['entities'] = [
            (entity,role)
            for entry       in context['results']['analyzed']
            for entity,role in entry['pos_tag']
            if role in (
                'NN','NNP','NNS','NNPS',
                'VB','VBD','VBN',
            )
        ]
        context['results']['topics'] = context['results']['entities']

        context['results']['matches'] = []

        for grammar in context['grammars']:
            grammar['book'] = '\n'.join(grammar['rules'])

            match = dict(
                alias   = grammar['name'],
                grammar = grammar['book'],
                corpus  = getattr(nltk.corpus, grammar['corpus'], nltk.corpus.brown),
                parser  = nltk.RegexpParser(grammar['book']),
            )

            match['result']   = [
                Reactor.nlp.flatten(item)
                for item in match['parser'].parse(context['results']['entities']).pos()
            ]
            match['recurse']  = match['parser'].parse(context['results']['topics']).pos()

            context['results']['matches'].append(match)

            context['results']['topics'] = match['recurse']

        context['results']['topics']   = [
            Reactor.nlp.flatten(item)
            for item in context['results']['topics']
        ]

        return context

    #***************************************************************************

    def express(self, request, context, data):
        context['results'] = {}

        datatype,mimetype = 'rdf','application/rdf+xml'
        datatype,mimetype = 'turtle','text/turtle'

        from rdflib.namespace import XMLNS,RDF,RDFS,XSD,OWL,SKOS,DOAP,FOAF,DC,DCTERMS,VOID

        raw = Reactor.sparql.mime('http://dbpedia.org/sparql', mimetype, data['statement'])

        grp = rdflib.Graph()
        grp.parse(format=datatype, data=raw)

        for row in grp.query(Reactor.rdf.NS+"""SELECT DISTINCT ?person ?name WHERE {
    ?person foaf:name ?name .
}"""):
            context['results']['people'].append(row)

        for row in grp.query(Reactor.rdf.NS+"""SELECT DISTINCT ?org ?title WHERE {
    ?org foaf:name ?title .
}"""):
            context['results']['organizations'].append(row)

        return context

