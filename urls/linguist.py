from django.conf.urls import include, url

from gestalt.web.utils import Reactor

from uchikoma.linguistics.views import linguist as Views

################################################################################

urlpatterns = Reactor.router.urlpatterns('linguist',
    #url(r'^express',     Views.rdf_express.as_view(),  name='express'),
    #url(r'^interpret',   Views.rdf_nlp.as_view(),      name='interpret'),
    #url(r'^enrich',      Views.rdf_enrich.as_view(),   name='enrich'),

    #url(r'^understand',  Views.nlp_sparql.as_view(),   name='understand'),
    #url(r'^tokenize',    Views.nlp_tokenize.as_view(), name='tokenizer'),

    #url(r'^$',           Views.Homepage.as_view(),     name='homepage'),
)

